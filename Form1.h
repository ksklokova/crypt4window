#include <windows.h>
#include <vcclr.h>

#pragma once


namespace cryptolab4window {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� Form1
	///
	/// ��������! ��� ��������� ����� ����� ������ ���������� ����� ��������
	///          �������� ����� ����� �������� ("Resource File Name") ��� �������� ���������� ������������ �������,
	///          ���������� �� ����� ������� � ����������� .resx, �� ������� ������� ������ �����. � ��������� ������,
	///          ������������ �� ������ ��������� �������� � ���������������
	///          ���������, ��������������� ������ �����.
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  buttonOK;
	private: System::Windows::Forms::TextBox^  textBoxInputFile;
	private: System::Windows::Forms::TextBox^  textBoxOutputFile;
	private: System::Windows::Forms::RadioButton^  radioButtonCaesar;
	private: System::Windows::Forms::RadioButton^  radioButtonVigenere;
	private: System::Windows::Forms::RadioButton^  radioButtonDES;
	private: System::Windows::Forms::RadioButton^  radioButtonBMP;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::CheckBox^  checkBoxDC;
	private: System::Windows::Forms::Label^  labelExtra;
	private: System::Windows::Forms::TextBox^  textBoxExtra;
	private: System::Windows::Forms::CheckBox^  checkBoxAlph;
	private: System::Windows::Forms::Button^  buttonInSearch;
	private: System::Windows::Forms::Button^  buttonOutSearch;
	private: System::Windows::Forms::Button^  buttonExtraSearch;
	private: System::Windows::Forms::OpenFileDialog^  openFileDialog1;





	protected: 

	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->buttonOK = (gcnew System::Windows::Forms::Button());
			this->textBoxInputFile = (gcnew System::Windows::Forms::TextBox());
			this->textBoxOutputFile = (gcnew System::Windows::Forms::TextBox());
			this->radioButtonCaesar = (gcnew System::Windows::Forms::RadioButton());
			this->radioButtonVigenere = (gcnew System::Windows::Forms::RadioButton());
			this->radioButtonDES = (gcnew System::Windows::Forms::RadioButton());
			this->radioButtonBMP = (gcnew System::Windows::Forms::RadioButton());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->checkBoxDC = (gcnew System::Windows::Forms::CheckBox());
			this->labelExtra = (gcnew System::Windows::Forms::Label());
			this->textBoxExtra = (gcnew System::Windows::Forms::TextBox());
			this->checkBoxAlph = (gcnew System::Windows::Forms::CheckBox());
			this->buttonInSearch = (gcnew System::Windows::Forms::Button());
			this->buttonOutSearch = (gcnew System::Windows::Forms::Button());
			this->buttonExtraSearch = (gcnew System::Windows::Forms::Button());
			this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->SuspendLayout();
			// 
			// buttonOK
			// 
			this->buttonOK->Location = System::Drawing::Point(92, 188);
			this->buttonOK->Name = L"buttonOK";
			this->buttonOK->Size = System::Drawing::Size(75, 23);
			this->buttonOK->TabIndex = 0;
			this->buttonOK->Text = L"OK";
			this->buttonOK->UseVisualStyleBackColor = true;
			this->buttonOK->Click += gcnew System::EventHandler(this, &Form1::buttonOK_Click);
			// 
			// textBoxInputFile
			// 
			this->textBoxInputFile->Location = System::Drawing::Point(105, 44);
			this->textBoxInputFile->Name = L"textBoxInputFile";
			this->textBoxInputFile->Size = System::Drawing::Size(100, 20);
			this->textBoxInputFile->TabIndex = 1;
			// 
			// textBoxOutputFile
			// 
			this->textBoxOutputFile->Location = System::Drawing::Point(105, 77);
			this->textBoxOutputFile->Name = L"textBoxOutputFile";
			this->textBoxOutputFile->Size = System::Drawing::Size(100, 20);
			this->textBoxOutputFile->TabIndex = 2;
			// 
			// radioButtonCaesar
			// 
			this->radioButtonCaesar->AutoSize = true;
			this->radioButtonCaesar->Location = System::Drawing::Point(13, 14);
			this->radioButtonCaesar->Name = L"radioButtonCaesar";
			this->radioButtonCaesar->Size = System::Drawing::Size(58, 17);
			this->radioButtonCaesar->TabIndex = 3;
			this->radioButtonCaesar->TabStop = true;
			this->radioButtonCaesar->Text = L"Caesar";
			this->radioButtonCaesar->UseVisualStyleBackColor = true;
			this->radioButtonCaesar->CheckedChanged += gcnew System::EventHandler(this, &Form1::radioButtonCaesar_CheckedChanged);
			// 
			// radioButtonVigenere
			// 
			this->radioButtonVigenere->AutoSize = true;
			this->radioButtonVigenere->Location = System::Drawing::Point(77, 14);
			this->radioButtonVigenere->Name = L"radioButtonVigenere";
			this->radioButtonVigenere->Size = System::Drawing::Size(67, 17);
			this->radioButtonVigenere->TabIndex = 4;
			this->radioButtonVigenere->TabStop = true;
			this->radioButtonVigenere->Text = L"Vigenere";
			this->radioButtonVigenere->UseVisualStyleBackColor = true;
			this->radioButtonVigenere->CheckedChanged += gcnew System::EventHandler(this, &Form1::radioButtonVigenere_CheckedChanged);
			// 
			// radioButtonDES
			// 
			this->radioButtonDES->AutoSize = true;
			this->radioButtonDES->Location = System::Drawing::Point(149, 14);
			this->radioButtonDES->Name = L"radioButtonDES";
			this->radioButtonDES->Size = System::Drawing::Size(47, 17);
			this->radioButtonDES->TabIndex = 5;
			this->radioButtonDES->TabStop = true;
			this->radioButtonDES->Text = L"DES";
			this->radioButtonDES->UseVisualStyleBackColor = true;
			this->radioButtonDES->CheckedChanged += gcnew System::EventHandler(this, &Form1::radioButtonDES_CheckedChanged);
			// 
			// radioButtonBMP
			// 
			this->radioButtonBMP->AutoSize = true;
			this->radioButtonBMP->Location = System::Drawing::Point(202, 14);
			this->radioButtonBMP->Name = L"radioButtonBMP";
			this->radioButtonBMP->Size = System::Drawing::Size(45, 17);
			this->radioButtonBMP->TabIndex = 6;
			this->radioButtonBMP->TabStop = true;
			this->radioButtonBMP->Text = L"bmp";
			this->radioButtonBMP->UseVisualStyleBackColor = true;
			this->radioButtonBMP->CheckedChanged += gcnew System::EventHandler(this, &Form1::radioButtonBMP_CheckedChanged);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(24, 51);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(50, 13);
			this->label1->TabIndex = 7;
			this->label1->Text = L"Input file:";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(24, 84);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(58, 13);
			this->label2->TabIndex = 8;
			this->label2->Text = L"Output file:";
			// 
			// checkBoxDC
			// 
			this->checkBoxDC->AutoSize = true;
			this->checkBoxDC->Location = System::Drawing::Point(13, 148);
			this->checkBoxDC->Name = L"checkBoxDC";
			this->checkBoxDC->Size = System::Drawing::Size(70, 17);
			this->checkBoxDC->TabIndex = 9;
			this->checkBoxDC->Text = L"Decode\?";
			this->checkBoxDC->UseVisualStyleBackColor = true;
			// 
			// labelExtra
			// 
			this->labelExtra->AutoSize = true;
			this->labelExtra->Location = System::Drawing::Point(24, 116);
			this->labelExtra->Name = L"labelExtra";
			this->labelExtra->Size = System::Drawing::Size(22, 13);
			this->labelExtra->TabIndex = 10;
			this->labelExtra->Text = L"     ";
			// 
			// textBoxExtra
			// 
			this->textBoxExtra->Location = System::Drawing::Point(105, 109);
			this->textBoxExtra->Name = L"textBoxExtra";
			this->textBoxExtra->Size = System::Drawing::Size(100, 20);
			this->textBoxExtra->TabIndex = 11;
			// 
			// checkBoxAlph
			// 
			this->checkBoxAlph->AutoSize = true;
			this->checkBoxAlph->Location = System::Drawing::Point(105, 148);
			this->checkBoxAlph->Name = L"checkBoxAlph";
			this->checkBoxAlph->Size = System::Drawing::Size(111, 17);
			this->checkBoxAlph->TabIndex = 12;
			this->checkBoxAlph->Text = L"Use alphabet file\?";
			this->checkBoxAlph->UseVisualStyleBackColor = true;
			this->checkBoxAlph->Visible = false;
			// 
			// buttonInSearch
			// 
			this->buttonInSearch->Location = System::Drawing::Point(202, 41);
			this->buttonInSearch->Name = L"buttonInSearch";
			this->buttonInSearch->Size = System::Drawing::Size(27, 23);
			this->buttonInSearch->TabIndex = 13;
			this->buttonInSearch->Text = L"...";
			this->buttonInSearch->UseVisualStyleBackColor = true;
			this->buttonInSearch->Click += gcnew System::EventHandler(this, &Form1::buttonInSearch_Click);
			// 
			// buttonOutSearch
			// 
			this->buttonOutSearch->Location = System::Drawing::Point(202, 75);
			this->buttonOutSearch->Name = L"buttonOutSearch";
			this->buttonOutSearch->Size = System::Drawing::Size(27, 23);
			this->buttonOutSearch->TabIndex = 14;
			this->buttonOutSearch->Text = L"...";
			this->buttonOutSearch->UseVisualStyleBackColor = true;
			this->buttonOutSearch->Click += gcnew System::EventHandler(this, &Form1::buttonOutSearch_Click);
			// 
			// buttonExtraSearch
			// 
			this->buttonExtraSearch->Location = System::Drawing::Point(202, 107);
			this->buttonExtraSearch->Name = L"buttonExtraSearch";
			this->buttonExtraSearch->Size = System::Drawing::Size(27, 23);
			this->buttonExtraSearch->TabIndex = 15;
			this->buttonExtraSearch->Text = L"...";
			this->buttonExtraSearch->UseVisualStyleBackColor = true;
			this->buttonExtraSearch->Visible = false;
			this->buttonExtraSearch->Click += gcnew System::EventHandler(this, &Form1::buttonExtraSearch_Click);
			// 
			// openFileDialog1
			// 
			this->openFileDialog1->FileName = L"openFileDialog1";
			this->openFileDialog1->Filter = L"my (*.txt;*.bmp;*.cpp)|*.txt;*.bmp;*.cpp";
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(251, 234);
			this->Controls->Add(this->buttonExtraSearch);
			this->Controls->Add(this->buttonOutSearch);
			this->Controls->Add(this->buttonInSearch);
			this->Controls->Add(this->checkBoxAlph);
			this->Controls->Add(this->textBoxExtra);
			this->Controls->Add(this->labelExtra);
			this->Controls->Add(this->checkBoxDC);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->radioButtonBMP);
			this->Controls->Add(this->radioButtonDES);
			this->Controls->Add(this->radioButtonVigenere);
			this->Controls->Add(this->radioButtonCaesar);
			this->Controls->Add(this->textBoxOutputFile);
			this->Controls->Add(this->textBoxInputFile);
			this->Controls->Add(this->buttonOK);
			this->MaximizeBox = false;
			this->Name = L"Form1";
			this->Text = L"Crypt";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void buttonOK_Click(System::Object^  sender, System::EventArgs^  e) {
			if (textBoxInputFile->Text!=""&&textBoxExtra->Text!="")
			{				
					STARTUPINFO cif;
					ZeroMemory(&cif,sizeof(STARTUPINFO));
					PROCESS_INFORMATION pi;
					System::String^ strApplication;
					System::String^ strComLine;
					bool trytorun=true;

					if (radioButtonCaesar->Checked
						&&textBoxOutputFile->Text!=""
						&&System::Convert::ToInt32(textBoxExtra->Text, 10)<256)
					{
						strApplication = "c:\\Crypto\\cryptlab1caesar.exe";
						strComLine = strApplication + " " + textBoxInputFile->Text + " -c";
						if (checkBoxDC->Checked) 
							strComLine+= " -dc " + textBoxExtra->Text + " " + textBoxOutputFile->Text;
						else
							strComLine+= " -cd " + textBoxExtra->Text + " " + textBoxOutputFile->Text;
						if (checkBoxAlph->Checked)
							strComLine+= " c:\\Crypto\\alphabet";
					}
					else if (radioButtonVigenere->Checked&&textBoxOutputFile->Text!="")
					{
						strApplication = "c:\\Crypto\\cryptlab1caesar.exe";
						strComLine = strApplication + " " + textBoxInputFile->Text + " -v";
						if (checkBoxDC->Checked) 
							strComLine+= " -dc " + textBoxExtra->Text + " " + textBoxOutputFile->Text;
						else
							strComLine+= " -cd " + textBoxExtra->Text + " " + textBoxOutputFile->Text;
						if (checkBoxAlph->Checked)
							strComLine+= " c:\\Crypto\\alphabet";
					}
					else if (radioButtonDES->Checked&&textBoxOutputFile->Text!="")
					{
						strApplication = "c:\\Crypto\\cryptlab2des.exe";
						strComLine = strApplication + " " + textBoxInputFile->Text + " " + textBoxOutputFile->Text;
						if (checkBoxDC->Checked) 
							strComLine+= " d " + textBoxExtra->Text;
						else
							strComLine+= " e " + textBoxExtra->Text;
					}
					else if (radioButtonBMP->Checked)
					{
						strApplication = "c:\\Crypto\\cryptlab3bmp.exe";
						strComLine = strApplication + " " + textBoxInputFile->Text;
						if (!checkBoxDC->Checked&&textBoxOutputFile->Text!="") 
							strComLine+= " c " + textBoxExtra->Text + " " + textBoxOutputFile->Text;
						else
							strComLine+= " e " + textBoxExtra->Text;
					}
					else trytorun=false;

					if(trytorun){
					pin_ptr<const wchar_t> __Application = PtrToStringChars(strApplication); 
					LPTSTR lpApplication = (LPTSTR) __Application;
					pin_ptr<const wchar_t> __ComLine = PtrToStringChars(strComLine); 
					LPTSTR lpComLine = (LPTSTR) __ComLine;

					if (CreateProcess(lpApplication, lpComLine,
						NULL,NULL,FALSE,NULL,NULL,NULL,&cif,&pi)==TRUE)
					{
						Sleep(1000);				// ���������
						TerminateProcess(pi.hProcess,NO_ERROR);	// ������ �������
					}}
			}
			else textBoxExtra->Text="FAIL!";
			 }
private: System::Void radioButtonCaesar_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (radioButtonCaesar->Checked)
				 {
					 labelExtra->Text="n:";
					 checkBoxAlph->Visible=true;
				 }
				 else
					 checkBoxAlph->Visible=false;
			 }


private: System::Void radioButtonVigenere_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (radioButtonVigenere->Checked)
				 {
					 labelExtra->Text="Key:";
					 checkBoxAlph->Visible=true;
				 }
				 else
					 checkBoxAlph->Visible=false;
		 }
private: System::Void radioButtonDES_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (radioButtonDES->Checked)
					 labelExtra->Text="Key:";
		 }
private: System::Void radioButtonBMP_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (radioButtonBMP->Checked)
				 {
					 labelExtra->Text="Message file:";
					 buttonExtraSearch->Visible=true;
				 }
				 else
					 buttonExtraSearch->Visible=false;
		 }
private: System::Void buttonInSearch_Click(System::Object^  sender, System::EventArgs^  e) {
			 	if(openFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK)
				  {
					 System::IO::StreamReader ^ sr = gcnew
						System::IO::StreamReader(openFileDialog1->FileName);
					 textBoxInputFile->Text = openFileDialog1->FileName;
					 sr->Close();
				  }
		 }
private: System::Void buttonOutSearch_Click(System::Object^  sender, System::EventArgs^  e) {
			 	if(openFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK)
				  {
					 System::IO::StreamReader ^ sr = gcnew
						System::IO::StreamReader(openFileDialog1->FileName);
					 textBoxOutputFile->Text = openFileDialog1->FileName;
					 sr->Close();
				  }
		 }
private: System::Void buttonExtraSearch_Click(System::Object^  sender, System::EventArgs^  e) {
			 	if(openFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK)
				  {
					 System::IO::StreamReader ^ sr = gcnew
						System::IO::StreamReader(openFileDialog1->FileName);
					 textBoxExtra->Text = openFileDialog1->FileName;
					 sr->Close();
				  }
		 }
};
}

